<?php

namespace MortaraEscribeXmlEditor;

class MortaraXml {

    public $sourceFile;
    public $document;

    function __construct($sourceFile){
        $this->sourceFile = $sourceFile;
        $this->document = new \DomDocument();
        $this->document->load($sourceFile);
    }

    public function anonymise(){
        foreach ($this->document->getElementsByTagName('DEMOGRAPHIC_FIELD') as $demographicElement) {
            if (in_array($demographicElement->getAttribute('ID'), [2, 7, 1])) {
                $demographicElement->setAttribute('VALUE', '');
            }
        }

        $this->document->getElementsByTagName('SUBJECT')->item(0)->setAttribute('FIRST_NAME', '');
        $this->document->getElementsByTagName('SUBJECT')->item(0)->setAttribute('LAST_NAME', '');
        $this->document->getElementsByTagName('SUBJECT')->item(0)->setAttribute('ID', '');
        return $this;
    }

    public function removeLeads($leadNames){

        foreach ($this->document->getElementsByTagName('TYPICAL_CYCLE_CHANNEL') as $demographicElement) {
            if (in_array($demographicElement->getAttribute('NAME'), $leadNames)) {
                $demographicElement->setAttribute('DATA', '');
            }
        }

        foreach ($this->document->getElementsByTagName('CHANNEL') as $demographicElement) {
            if (in_array($demographicElement->getAttribute('NAME'), $leadNames)) {
                $demographicElement->setAttribute('DATA', '');
            }
        }

        return $this;
    }

    public function copy(){
        return $this;
    }

    public function save($targetFile){
        $this->document->save($targetFile);
    }


}